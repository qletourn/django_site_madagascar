from django import forms

class inputsForm(forms.Form):
    reads_field = forms.FileField(label="Paired-end reads", widget=forms.ClearableFileInput(attrs={'multiple': True, 'accept':'.fastq,.fq,.fastq.gz,.fq.gz'}), help_text="Select your input reads files")
    min_length = forms.IntegerField(label="Min reads length", help_text="Reads with length inferior to this value will not be used for assembly", initial=45, min_value=0)
    assembler = forms.ChoiceField(label="Assembler", choices=(('metaspades','MetaSpades'),
            ('megahit','Megahit'),
            ('ray','Ray meta')),
    help_text="Select the wanted assembler")
    vp1=forms.IntegerField(label="Minimium coverage for annotation", initial=50, min_value =0, help_text="Coverage threshsold to annotate VP1 sequences")
    p1=forms.IntegerField(initial=50, min_value=0, help_text="Coverage threshsold to annotate P1 sequences")
    utr=forms.IntegerField(initial=50, min_value=0, help_text="Coverage threshsold to annotate 5UTR sequences")
    threeD=forms.IntegerField(initial=50, min_value=0, help_text="Coverage threshsold to annotate 3D sequences")
    eval=forms.FloatField(label="BLAST e-value", initial=0.001, min_value=0, help_text="")
    mail=forms.EmailField(label="Email", help_text="You will receive a notification when results are ready")
