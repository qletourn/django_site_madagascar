import os, json, shutil, string, numpy as np
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
from django.core.files.storage import FileSystemStorage
from .forms import inputsForm

FILE_PATH="/home/qletourn/Documents/Test/"

# Create your views here.
def home(request):
    template = loader.get_template("interface/home.html")
    return HttpResponse(template.render())

# def run(request):
#     template = loader.get_template("interface/run.html")
#     return HttpResponse(template.render())

def run(request):
    global FILE_PATH
    if request.method == 'POST':
        form = inputsForm(request.POST, request.FILES)
        if form.is_valid():
            id = ''.join(np.random.choice(list(string.ascii_lowercase + string.digits), 20, replace = True))
            
            files = request.FILES.getlist('reads_field')
            inputs = request.POST
            inputs_dict = dict(inputs)
            inputs_dict.pop('csrfmiddlewaretoken', None)
            inputs_dict['id'] = id
            
            # os.makedirs(file_path)
            for f in files:
                if 'files' not in inputs_dict:
                    inputs_dict['files'] = [f.name]
                else:
                    inputs_dict['files'].append(f.name)
                shutil.move(f.temporary_file_path(), "{}/{}".format(FILE_PATH, f.name))
            # print(f.temporary_file_path(), f.name)
            
            with open('{}/{}.json'.format(FILE_PATH, id), 'w') as of:
                json.dump(inputs_dict, of)
            
            return redirect('interface:myres', id=id)
        else:
            form = inputsForm()
            return HttpResponse(render(request, 'interface/run.html', {'title':'RUN',
            'form':form}))
    else:
        form = inputsForm()
        return HttpResponse(render(request, 'interface/run.html', {'title':'RUN',
        'form':form}))

def res(request, id):
    global FILE_PATH
    if os.path.exists("{}/DONE/{}.zip".format(FILE_PATH, id)):
        tab = [['Month','Savings'],['January', 100], ['February', 50]]
        context = {'title': 'RESULTS', 'id': id, 'tab': tab}
    else:
        context = {'title': 'RUNNING'}
    return render(request, 'interface/exec.html', context)

def dl(request, id):
    global FILE_PATH
    with open("{}/DONE/{}.zip".format(FILE_PATH, id), 'rb') as fh:
        response = HttpResponse(fh.read(), content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename='+id+'.zip'
        return response
    raise Http404
