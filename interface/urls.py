from django.urls import re_path, path
from django.conf.urls import url

from interface import views

app_name = "interface"
urlpatterns = [
    re_path(r'^$', views.home, name='myhome'),
    re_path(r'^run$', views.run, name='myrun'),
    re_path(r'^res/(?P<id>[\d\w]+)$', views.res, name='myres'),
    re_path(r'^res/dl/(?P<id>[\d\w]+)$', views.dl, name='mydl')
]
